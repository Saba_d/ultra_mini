package app.carematix.ultra_mini;

/**
 * Created by Sindy on 12/17/14.
 */
public class template {
    public static void main(String[] args)
    {
        template_method();
    }
    public static void template_method()
    {
      String template =  (
              "00000010" + //STX
              "00001000" + //Length
              "00010100" + //Link Control byte
              "00000004" + //Data
              "00000005" + //Data
              "00000011" + //ETX
              "00000007" + //CRC Low
              "00000008" //CRC high
      );

        String sub = template.substring(8, 16); //substring of "Length"
        int decimal = Integer.parseInt(""+sub, 2); //Calculating value of "sub" in decimal

        String[] tokens = template.split("(?<=\\G.{8})"); //splitting parts of template string in to 8 bit strings
        int length = decimal-1; //actual length of the string will be decimal-1 because of the index of "0"

        for(int i=0; i<=length; i++) {
            int x = i+1;
            System.out.println(""+tokens[i]+"..."+x); //print out all the separated Strings
        }
        parse_basic_info(tokens,length);
    }

    public static void parse_basic_info(String tokens[], int length)
    {

             String link_control_byte=tokens[2];
             String crc_low=tokens[length-1];
             String crc_high=tokens[length];

      //!!!!!! Parsing Link control byte!!!!!!
         System.out.println("link_control_byte: "+link_control_byte+" crc_low: "+crc_low+" crc_high: "+crc_high);
           if(link_control_byte.charAt(3)=='1')
           {
               System.out.println("More data incoming");
           }
        if(link_control_byte.charAt(4)=='1')
        {
            System.out.println("Disconnect");
        }
        if(link_control_byte.charAt(5)=='1')
        {
            System.out.println("Acknowledged");
        }
    }
}
