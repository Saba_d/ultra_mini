package app.carematix.ultra_mini;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by selithey on 11/5/14.
 */

public class DBAdapter {
    public static final String KEY_ROWID = "ID";
    public static final String KEY_RR = "RawReadings";
    public static final String KEY_SNO = "SerialNo";
    public static final String KEY_MOD = "Model";
    public static final String KEY_MANU = "Manufacturer";
    public static final String KEY_TS = "TimeStamp";
    public static final String KEY_RV = "ReadingValue";
    public static final String KEY_TF = "TransmittedFlag";

    private static final String TAG = "DBAdapter";

    private static final String DATABASE_NAME = "UltraMiniDB";
    private static final String DATABASE_TABLE = "Readings";
    private static final int DATABASE_VERSION = 2;

    private static final String DATABASE_CREATE = "create table if not exists Readings(ID integer primary key autoincrement,"
            + "RawReadings VARCHAR not null, SerialNo VARCHAR, Model VARCHAR, Manufacturer VARCHAR, TimeStamp VARCHAR, ReadingValue VARCHAR,"
            + "TransmittedFlag BOOLEAN);";

    private final Context context;

    private DatabaseHelper DBHelper;
    private SQLiteDatabase db;

    public DBAdapter(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);

    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                db.execSQL(DATABASE_CREATE);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version" + oldVersion + " to " + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS Readings");
            onCreate(db);
        }
    }

    public DBAdapter open() throws SQLException {
        db = DBHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        DBHelper.close();
    }

    public long insertRecord(String RawReadings, String SerialNo, String Model, String Manufacturer, String TimeStamp, String ReadingValue, String TransmittedFlag) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_RR, RawReadings);
        initialValues.put(KEY_SNO, SerialNo);
        initialValues.put(KEY_MOD, Model);
        initialValues.put(KEY_MANU, Manufacturer);
        initialValues.put(KEY_TS, TimeStamp);
        initialValues.put(KEY_RV, ReadingValue);
        initialValues.put(KEY_TF, TransmittedFlag);
        System.out.println("Success");

        return db.insert(DATABASE_TABLE, null, initialValues);
    }

    public Cursor getRecord(long rowId) throws SQLException {
        Cursor mCursor =
                db.query(true, DATABASE_TABLE, new String[]{KEY_ROWID, KEY_RR, KEY_SNO, KEY_MOD, KEY_MANU, KEY_TS, KEY_RV, KEY_TF},
                        KEY_ROWID + "=" + rowId, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public Cursor getAllRecords() {
        return db.query(DATABASE_TABLE, new String[]{KEY_ROWID, KEY_RR, KEY_SNO, KEY_MOD, KEY_MANU, KEY_TS, KEY_RV, KEY_TF}, null, null, null, null, null);
    }

        /*
        public boolean updateRecord(long rowId, String RawReadings, String SerialNo, String Model, String Manufacturer, String TimeStamp, String ReadingValue, String TransmittedFlag)
        {
            ContentValues args = new ContentValues();
            args.put(KEY_RR, RawReadings);
            args.put(KEY_SNO, SerialNo);
            args.put(KEY_MOD, Model);
            args.put(KEY_MANU, Manufacturer);
            args.put(KEY_TS, TimeStamp);
            args.put(KEY_RV, ReadingValue);
            args.put(KEY_TF, TransmittedFlag);
            return db.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
        }*/

        public void deleteall()
        {
            db.delete(DATABASE_TABLE,null,null);
        }



}



