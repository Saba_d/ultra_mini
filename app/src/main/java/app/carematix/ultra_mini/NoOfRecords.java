package app.carematix.ultra_mini;

import android.content.Context;
import app.carematix.ultra_mini.DBAdapter;

/**
 * Created by selithey on 11/22/14.
 */
public class NoOfRecords {
    Context context;
    DBAdapter db = new DBAdapter(context);


    public  void cmd_frm_pc_1()
    {
        String command =  (
                "00000010" + //STX
                        "00001010" + //Length
                        "00000000" + //Link Control byte
                        "00000101" + //CM1
                        "00001111" + //CM2
                        "00000001" + //501
                        "11110101" + //501
                        "00000011" + //ETX
                        "11011010" + //CRC Low
                        "01110001" //CRC high
        );
    }
    public  void ack_frm_meter_2()
    {
        String command =  (
                "00000010" + //STX
                        "00000110" + //Length
                        "00000110" + //Link Control byte
                        "00000011" + //ETX
                        "11001101" + //CRC Low
                        "01000001" //CRC high
        );
    }
    public static void data_frm_meter_3()
    {
        String command =  (
                "00000010" + //STX
                        "00011010" +//Length
                        "00001010" + //Link Control byte
                        "00000101" + //RM1
                        "00001111" + //RM2
                        "00000000" + //No_of_Records
                        "00000101" + //No_of_Records
                        "00000011" + //ETX
                        "00011100" + //CRC Low
                        "01011000" //CRC high
        );

        String sub = command.substring(8, 16); //substring of "Length"
        int decimal = Integer.parseInt(""+sub, 2); //Calculating value of "sub" in decimal

        String[] tokens = command.split("(?<=\\G.{8})"); //splitting parts of template string in to 8 bit strings
        int length = decimal-1; //actual length of the string will be decimal-1 because of the index of "0"

        for(int i=0; i<=length; i++) {
            int x = i+1;
//            System.out.println(""+tokens[i]+"..."+x); //print out all the separated Strings
            //       System.out.println(""+i+"..."+length);
            parse_data(tokens, length);
        }

        // parse_basic_info(tokens,length);

    }


    public  void ack_frm_pc_4()
    {
        String command =  (
                "00000010" + //STX
                        "00000110" + //Length
                        "00000111" + //Link Control byte
                        "00000011" + //ETX
                        "11111100" + //CRC Low
                        "01110010" //CRC high
        );
    }
    public  void parse_basic_info(String[] tokens, int length)
    {
        String link_control_byte=tokens[2];
        String crc_low=tokens[length-1];
        String crc_high=tokens[length];
    }

    public static void parse_data(String[] tokens, int length)
    {
        String no_of_records=""+BinaryToAscii(tokens[5])+""+BinaryToAscii(tokens[6]);
        String software_version = ""+BinaryToAscii(no_of_records);
        System.out.println(software_version);
       /* db.open();
        db.insertRecord("", "", software_version, creation_date, "","", "");
        db.close();*/


    }
    public static String  BinaryToAscii(String convert)
    {
        int charCode = Integer.parseInt(convert, 2);
        String converted = new Character((char)charCode).toString();
        return converted;
    }
    public static void main(String[] args)
    {
        data_frm_meter_3();
    }

}
