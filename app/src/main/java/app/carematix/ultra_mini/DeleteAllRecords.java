package app.carematix.ultra_mini;

import android.content.Context;
import app.carematix.ultra_mini.DBAdapter;

/**
 * Created by selithey on 11/22/14.
 */
public class DeleteAllRecords {
    Context context;
    DBAdapter db = new DBAdapter(context);


    public  void cmd_frm_pc_1()
    {
        String command =  (
                "00000010" + //STX
                        "00001000" + //Length
                        "00000000" + //Link Control byte
                        "00000101" + //CM1
                        "0001 1010" + //CM2
                        "00000011" + //ETX
                        "0101 0110" + //CRC Low
                        "1011 0000" //CRC high
        );
    }
    public  void ack_frm_meter_2()
    {
        String command =  (
                "00000010" + //STX
                        "00000110" + //Length
                        "00000110" + //Link Control byte
                        "00000011" + //ETX
                        "11001101" + //CRC Low
                        "01000001" //CRC high
        );
    }
    public void executed_frm_meter_3()
    {
        String command =  (
                "00000010" + //STX
                        "00001000" +//Length
                        "00000010" + //Link Control byte
                        "00000101" + //RM1
                        "00000110" + //RM2
                        "00000011" + //ETX
                        "0010 0000" + //CRC Low
                        "0001 1011" //CRC high
        );

        String sub = command.substring(8, 16); //substring of "Length"
        int decimal = Integer.parseInt(""+sub, 2); //Calculating value of "sub" in decimal

        String[] tokens = command.split("(?<=\\G.{8})"); //splitting parts of template string in to 8 bit strings
        int length = decimal-1; //actual length of the string will be decimal-1 because of the index of "0"

        for(int i=0; i<=length; i++) {
            int x = i+1;
//            System.out.println(""+tokens[i]+"..."+x); //print out all the separated Strings
            //       System.out.println(""+i+"..."+length);
            parse_data(tokens, length);
        }

        // parse_basic_info(tokens,length);

    }


    public  void ack_frm_pc_4()
    {
        String command =  (
                "00000010" + //STX
                        "00000110" + //Length
                        "00000111" + //Link Control byte
                        "00000011" + //ETX
                        "11111100" + //CRC Low
                        "01110010" //CRC high
        );
    }
    public  void parse_basic_info(String[] tokens, int length)
    {
        String link_control_byte=tokens[2];
        String crc_low=tokens[length-1];
        String crc_high=tokens[length];
    }

    public void parse_data(String[] tokens, int length)
    {
        /*String software_version = ""+BinaryToAscii(tokens[6])+""+BinaryToAscii(tokens[7])+""+BinaryToAscii(tokens[8])+""+BinaryToAscii(tokens[9])+""+BinaryToAscii(tokens[10])+""+BinaryToAscii(tokens[11])+""+BinaryToAscii(tokens[12])+""+BinaryToAscii(tokens[13])+""+BinaryToAscii(tokens[14]);
        String creation_date=""+BinaryToAscii(tokens[15])+""+BinaryToAscii(tokens[16])+""+BinaryToAscii(tokens[17])+""+BinaryToAscii(tokens[18])+""+BinaryToAscii(tokens[19])+""+BinaryToAscii(tokens[20])+""+BinaryToAscii(tokens[21])+""+BinaryToAscii(tokens[22]);
        System.out.println(software_version);
        System.out.println(creation_date);*/

      /*  db.open();
        db.deleteall();
        System.out.println("All records DELETED");
        db.close();
*/

    }
    public  String  BinaryToAscii(String convert)
    {
        int charCode = Integer.parseInt(convert, 2);
        String converted = new Character((char)charCode).toString();
        return converted;
    }

}
