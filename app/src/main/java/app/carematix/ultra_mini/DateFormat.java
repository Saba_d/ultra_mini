package app.carematix.ultra_mini;

import android.content.Context;

import app.carematix.ultra_mini.DBAdapter;

/**
 * Created by selithey on 11/22/14.
 */
public class DateFormat {
    Context context;
    DBAdapter db = new DBAdapter(context);


    public void cmd_frm_pc_1() {
        String command = (
                "00000010" + //STX
                        "0000 1110" + //Length
                        "00000000" + //Link Control byte
                        "00000101" + //CM1
                        "00001000" + //CM2
                        "00000010" + //CM3
                        "00000000" + //CM4
                        "00000000" + //PM1
                        "00000000" + //PM2
                        "00000000" + //PM3
                        "00000000" + //PM4
                        "00000011" + //ETX
                        "11111111" + //CRC Low
                        "11101000" //CRC high
        );
    }

    public void ack_frm_meter_2() {
        String command = (
                "00000010" + //STX
                        "00000110" + //Length
                        "00000110" + //Link Control byte
                        "00000011" + //ETX
                        "11001101" + //CRC Low
                        "01000001" //CRC high
        );
    }

    public void data_frm_meter_3() {
        String command = (
                "00000010" + //STX
                        "0000 1100" +//Length
                        "00000010" + //Link Control byte
                        "00000101" + //RM1
                        "00000110" + //RM2
                        "00000001" + //PM1
                        "00000000" + //PM2
                        "00000000" + //PM3
                        "00000000" + //PM4
                        "00000011" + //ETX
                        "0111 0001" + //CRC Low
                        "0110 1011" //CRC high
        );

        String sub = command.substring(8, 16); //substring of "Length"
        int decimal = Integer.parseInt("" + sub, 2); //Calculating value of "sub" in decimal

        String[] tokens = command.split("(?<=\\G.{8})"); //splitting parts of template string in to 8 bit strings
        int length = decimal - 1; //actual length of the string will be decimal-1 because of the index of "0"

        for (int i = 0; i <= length; i++) {
            int x = i + 1;
//            System.out.println(""+tokens[i]+"..."+x); //print out all the separated Strings
            //       System.out.println(""+i+"..."+length);
            parse_data(tokens, length);
        }

        // parse_basic_info(tokens,length);

    }


    public void ack_frm_pc_4() {
        String command = (
                "00000010" + //STX
                        "00000110" + //Length
                        "00000111" + //Link Control byte
                        "00000011" + //ETX
                        "11111100" + //CRC Low
                        "01110010" //CRC high
        );
    }

    public void parse_basic_info(String[] tokens, int length) {
        String link_control_byte = tokens[2];
        String crc_low = tokens[length - 1];
        String crc_high = tokens[length];
    }

    public void parse_data(String[] tokens, int length) {
        if (Integer.parseInt(tokens[5], 2) == 0) {
            System.out.println("US date format");
        } else {
            System.out.println("EU date format");
        }


    }


}
