package app.carematix.ultra_mini;

import android.content.Context;

import java.util.Date;

import app.carematix.ultra_mini.DBAdapter;

/**
 * Created by selithey on 11/22/14.
 */
public class ReadRecord_x {
    Context context;
    DBAdapter db = new DBAdapter(context);


    public void cmd_frm_pc_1() {
        String command = (
                "00000010" + //STX
                        "00001010" + //Length
                        "00000011" + //Link Control byte
                        "00000101" + //CM1
                        "00011111" + //CM2
                        "00000000" + //Record x : 1
                        "00000000" + //Record x : 1
                        "00000011" + //ETX
                        "01001011" + //CRC Low
                        "01011111" //CRC high
        );
    }

    public void ack_frm_meter_2() {
        String command = (
                "00000010" + //STX
                        "00000110" + //Length
                        "00000110" + //Link Control byte
                        "00000011" + //ETX
                        "11001101" + //CRC Low
                        "01000001" //CRC high
        );
    }

    public static void data_frm_meter_3() {
        String command = (
                "00000010" + //STX
                        "00001010" +//Length
                        "00000001" + //Link Control byte
                        "00000101" + //RM1
                        "00000110" + //RM2
                        "10101100" + //DT1
                        "10000110" + //DT2
                        "01010101" + //DT3
                        "01101000" + //DT4
                        "01001100" + //GR1
                        "00000000" + //GR2
                        "00000000" + //GR3
                        "00000000" + //GR4
                        "00000011" + //ETX
                        "01011101" + //CRC Low
                        "01100000" //CRC high
        );

        String sub = command.substring(8, 16); //substring of "Length"
        int decimal = Integer.parseInt("" + sub, 2); //Calculating value of "sub" in decimal

        String[] tokens = command.split("(?<=\\G.{8})"); //splitting parts of template string in to 8 bit strings
        int length = decimal - 1; //actual length of the string will be decimal-1 because of the index of "0"

        for (int i = 0; i <= length; i++) {
            int x = i + 1;
            System.out.println("" + tokens[i] + "..." + x); //print out all the separated Strings
            System.out.println("" + i + "..." + length);
            parse_data(tokens, length);
        }

        // parse_basic_info(tokens,length);

    }


    public void ack_frm_pc_4() {
        String command = (
                "00000010" + //STX
                        "00000110" + //Length
                        "00000111" + //Link Control byte
                        "00000011" + //ETX
                        "11111100" + //CRC Low
                        "01110010" //CRC high
        );
    }

    public void parse_basic_info(String[] tokens, int length) {
        String link_control_byte = tokens[2];
        String crc_low = tokens[length - 1];
        String crc_high = tokens[length];
    }

    public static void parse_data(String[] tokens, int length) {

        String hex = "" + BinarytoHex(tokens[8]) + "" + BinarytoHex(tokens[7]) + "" + BinarytoHex(tokens[6]) + "" + BinarytoHex(tokens[5]);
        String timeStamp = ""+parseDate(hex);

        String glucose_value = "" + Integer.parseInt("" + tokens[9], 2) /*+ "" + Integer.parseInt("" + tokens[10], 2) + "" + Integer.parseInt("" + tokens[11], 2) + "" + Integer.parseInt("" + tokens[12], 2)*/;
        System.out.println(timeStamp);
        System.out.println(glucose_value);
       /* db.open();
        db.insertRecord("", "", software_version, creation_date, "","", "");
        db.close();
*/

    }

    public static Date parseDate(String hex) {
        int date = Integer.parseInt(hex, 16);
        Date timeStamp = new Date(Long.parseLong(""+date)*1000);
        return timeStamp;
    }

    public static String BinarytoHex(String conv) {
        int x = Integer.parseInt(conv, 2);
        String hex = Integer.toHexString(x);
        return hex;
    }

    public static void main(String[] args) {
        data_frm_meter_3();
    }
}
