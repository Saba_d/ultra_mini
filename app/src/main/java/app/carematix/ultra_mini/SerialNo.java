package app.carematix.ultra_mini;

import android.content.Context;

import java.util.Scanner;

import app.carematix.ultra_mini.DBAdapter;

/**
 * Created by selithey on 11/22/14.
 */
public class SerialNo {
    Context context;
    DBAdapter db = new DBAdapter(context);


    public  void cmd_frm_pc_1()
    {
        String command =  (
                "00000010" + //STX
                        "0001 0010" + //Length
                        "00000000" + //Link Control byte
                        "00000101" + //CM1
                        "0000 1011" + //CM2
                        "00000010" + //CM3
                        "00000000" + //CM4
                        "00000000" + //CM5
                        "00000000" + //CM6
                        "00000000" + //CM7
                        "10000100" + //CM8
                        "01101010" + //CM9
                        "11101000" + //CM10
                        "01110011" + //CM11
                        "00000000" + //CM12
                        "00000011" + //ETX
                        "10011011" + //CRC Low
                        "11101010" //CRC high
        );
    }
    public  void ack_frm_meter_2()
    {
        String command =  (
                "00000010" + //STX
                        "00000110" + //Length
                        "00000110" + //Link Control byte
                        "00000011" + //ETX
                        "11001101" + //CRC Low
                        "01000001" //CRC high
        );
    }
    public static void data_frm_meter_3()
    {
        String command =  (
                "00000010" + //STX
                        "00011010" +//Length
                        "00000010" + //Link Control byte
                        "00000101" + //CM1
                        "00000110" + //CM2
                        "01000011" + //C
                        "00110001" + //1
                        "00110111" + //7
                        "00110110" + //6
                        "01010011" + //S
                        "01000001" + //A
                        "00110000" + //0
                        "01001111" + //O
                        "00110000" + //0
                        "00000011" + //ETX
                        "01001001" + //CRC Low
                        "01000011" //CRC high
        );

        String sub = command.substring(8, 16); //substring of "Length"
        int decimal = Integer.parseInt(""+sub, 2); //Calculating value of "sub" in decimal

        String[] tokens = command.split("(?<=\\G.{8})"); //splitting parts of template string in to 8 bit strings
        int length = decimal-1; //actual length of the string will be decimal-1 because of the index of "0"

        for(int i=0; i<=length; i++) {
            int x = i+1;
//            System.out.println(""+tokens[i]+"..."+x); //print out all the separated Strings
            //       System.out.println(""+i+"..."+length);
            parse_data(tokens, length);
            String lcb = tokens[2];
            parse_link_control_byte(lcb);
        }

        // parse_basic_info(tokens,length);

    }


    public  void ack_frm_pc_4()
    {
        String command =  (
                "00000010" + //STX
                        "00000110" + //Length
                        "00000111" + //Link Control byte
                        "00000011" + //ETX
                        "11111100" + //CRC Low
                        "01110010" //CRC high
        );
    }
    public  void parse_basic_info(String[] tokens, int length)
    {
        String link_control_byte=tokens[2];
        String crc_low=tokens[length-1];
        String crc_high=tokens[length];
    }

    public static void parse_data(String[] tokens, int length)
    {
        String serial_number = ""+BinaryToAscii(tokens[5])+""+BinaryToAscii(tokens[6])+""+BinaryToAscii(tokens[7])+""+BinaryToAscii(tokens[8])+""+BinaryToAscii(tokens[9])+""+BinaryToAscii(tokens[10])+""+BinaryToAscii(tokens[11])+""+BinaryToAscii(tokens[12])+""+BinaryToAscii(tokens[13]);
        System.out.println(serial_number);
       /* db.open();
        db.insertRecord("", serial_number, "", "", "","", "");
        db.close();*/


    }
    public static void parse_link_control_byte(String lcb)
    {  System.out.println("Enter position: ");
        Scanner reader = new Scanner(System.in);
        int position = reader.nextInt();
        position = position+1;
        String link_info;
        switch (position) {
            case 1:
                link_info = "Unused";
                break;
            case 2:  link_info = "Unused";
                break;

            case 3:  link_info = "Unused";
                break;
            case 4:  char result1 = lcb.charAt(3);
                if (result1=='1')
                {
                    link_info = "More dataframe";
                }
                else
                {
                    link_info = "No-More dataframe";
                }
                break;
            case 5:  char result2 = lcb.charAt(4);
                if (result2=='1')
                {
                    link_info = "Disconnect";
                }
                else
                {
                    link_info = "No-Disconnect";
                }
                break;
            case 6:   char result3 = lcb.charAt(5);
                if (result3=='1')
                {
                    link_info = "Acknowledge";
                }
                else
                {
                    link_info = "No-Acknowledge";
                }
                break;
            case 7:  char result4 = lcb.charAt(6);
                if (result4=='1')
                {
                    link_info = "Expected Receive";
                }
                else
                {
                    link_info = "Data Link Set";
                }
                break;
            case 8:   char result5 = lcb.charAt(7);
                if (result5=='1')
                {
                    link_info = "Acknowledgement Received";
                }
                else
                {
                    link_info = "Message Transmitted";
                }
                break;

            default: link_info = "Invalid position";
                break;
        }
        System.out.println(link_info);
    }
    public static String  BinaryToAscii(String convert)
    {
        int charCode = Integer.parseInt(convert, 2);
        String converted = new Character((char)charCode).toString();
        return converted;
    }

    public static void main(String args[])
    {
        data_frm_meter_3();

    }

}
