package app.carematix.ultra_mini;

import android.content.Context;

/**
 * Created by selithey on 11/21/14.
 */
public class dbInsertion {
    Context context;
    DBAdapter db = new DBAdapter(context);

    public Boolean insertRecord(String RawReadings, String SerialNo, String Model, String Manufacturer,
                                String TimeStamp, String ReadingValue, String TransmittedFlag)
    {
        // insertRecord(String RawReadings, String SerialNo, String
        // Model, String Manufacturer, String TimeStamp, String ReadingValue, String TransmittedFlag)
        db.open();
        long id = db.insertRecord(RawReadings, SerialNo, Model, Manufacturer, TimeStamp, ReadingValue, TransmittedFlag);
        db.close();
        return true;
    }
}
