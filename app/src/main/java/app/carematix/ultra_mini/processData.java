package app.carematix.ultra_mini;

import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.lang.*;

/**
 * Created by selithey on 11/5/14.
 */

public class processData {

    public void splitString(){
        String dataString=("0000001000000111000000030000000400000005000000110000000700000008");
        String sub = dataString.substring(8, 16);
        int decimal = Integer.parseInt(""+sub, 2);
        Log.e("",""+decimal);
        String text = dataString;
        String[] tokens = text.split("(?<=\\G.{8})");
        int length = decimal-1;
        for(int i=0; i<=length; i++) {
            Log.e("",""+tokens[i]+"..."+i);
        }
        checkRecord(tokens, length);

    }
    public static void checkRecord(String[] tokens, int length)
    {
        Log.e("","!");
        if(tokens[0].equals("00000010"))
        {
            if(tokens[length-2].equals("00000011"))
            {
                Log.e("","Data received");
            }
            else {
                Log.e("","Error in receiving data: ETX");
                Log.e("",""+tokens[length-2]);
            }
        }
        else {
            Log.e("","Error in receiving data: STX");
            Log.e("",""+tokens[0]);
        }
    }

    public int hexTodecimal(String input)
    {
        int temp1;
        temp1 = Integer.parseInt(input, 16 );

        return temp1;
    }

    public String epochConv(int epoch)
    {
        long currentDateTime = epoch;

        Date currentDate = new Date(currentDateTime);

        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(currentDateTime);

        String x = (""+df.format(cal.getTime()));
        return x;
    }
}
